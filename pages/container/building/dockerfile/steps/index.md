# Step Deep Dive

Let's use the following (simple) Dockerfile to dive deeper into what happens when you build an image.

```{ .bash .copy }
cat > Dockerfile << EOF
FROM alpine:3.18.0
ENV KEY=value
RUN apk add --quiet py3-pip \
 && pip install --quiet cowsay
CMD ["cowsay", "-t", "Hello World"]
EOF
```

I'd like to use the non-buildkit output to explain what is going on. 

First, each line with a statement within a Dockerfile is called a `step`. Each step results in a new container image.  
In the old output (non-buildkit) it can be nicely seen as `---> <id>`. The `<id>` is the hashsum of the resulting image.

Each step will do something like this:

1. `docker run --no-healthcheck [container options] <previous-id> [bash -c "<statement>"]`  
   This will create a new container from the previous image and either executes the statement (`RUN`) within the container or just change the runtime configuration (`ENV`, `CMD`, etc).
2. `docker commit <container-id>`  
   This will commit the container with all changes into a new image. The new image will be the base image for the next step.
3. `docker rm <container-id>`  
   This will remove the container as it is not needed anymore.

Please check out the annotation within the output below. They will show the equivalent `docker run` command that is executed.

```{ .bash .no-copy }
Sending build context to Docker daemon  3.288MB
Step 1/3 : FROM alpine:3.18.0 # (1)!
---> 44dd6f223004
Step 2/4 : ENV KEY=value # (2)!
 ---> Running in 553e18f8b3ad
Removing intermediate container 553e18f8b3ad
 ---> 34a658bf6255
Step 3/4 : RUN apk add --quiet py3-pip  && pip install --quiet cowsay # (3)!
 ---> Running in d849133b6473
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv
Removing intermediate container d849133b6473
 ---> b5db91e1c97d
Step 4/4 : CMD ["cowsay", "-t", "Hello World"] # (4)!
 ---> Running in 98ad5bf0e85e
Removing intermediate container 98ad5bf0e85e
 ---> d4d61ab0346e
Successfully built d4d61ab0346e
Successfully tagged test:latest
```

1. `docker run alpine:3.18.0`  
   The first one is rather simple. It just uses the `alpine:3.18.0` image as the base image.
2. `docker run -e KEY=value 44dd6f223004`  
   This step does not change the container file-system. It just adds an environment variable to the container.
3. `docker run 34a658bf6255 /bin/sh -c 'apk add --quiet py3-pip  && pip install --quiet cowsay'`  
   This step does change the container file-system. It installs the `py3-pip` package and the `cowsay` package.
4. `docker run b5db91e1c97d cowsay -t Hello World`  
   Again, not changing the container file-system but instead set the `Cmd` statement of the container.
