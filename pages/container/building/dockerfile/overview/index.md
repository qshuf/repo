# Dockerfile

A Dockerfile describes steps to create a container image. Those steps include metadata changes (environments, healthchecks) and filesystem changes (installing packages, copying files, etc).

## Hello World

```{ .bash .copy }
cat > Dockerfile << EOF
# (1)!
FROM alpine:3.18.0
# (2)!
ENV KEY=value
# (3)!
RUN apk add --quiet py3-pip \
 && pip install --quiet cowsay
# (4)!
CMD ["cowsay", "-t", "Hello World"]
EOF
```

1. `FROM` statement to define the base image. The base-image defines the user-land (operating system) of the contianer and the configuration thus far (e.g. environment, command, etc).
2. The `ENV` statement defines an environment variable. In this case, we define a variable `KEY` with the value `value`.
3. The `RUN` statement executes a command within the container. In this case, we install the `pip` and the `cowsay` package.
4. The `CMD` statement defines the default command to be executed when the container is started.

To build the image we use the `docker build` command. The first tab shows the command without using [buildkit]() as it has a cleaner output. The second tab shows the command with buildkit enabled.

=== "W/o BuildKit"
    ```{ .bash .copy }
    DOCKER_BUILDKIT=0 docker build --no-cache -t test .
    ```
    The command will be deprecated eventually. I like to keep it here as it helps explain what is going on more clearly.
    
    ```{ .bash .no-copy }
    $ DOCKER_BUILDKIT=0 docker build --no-cache -t test .
    Sending build context to Docker daemon  3.288MB
    Step 1/4 : FROM alpine:3.18.0
    ---> 44dd6f223004
    Step 2/4 : ENV KEY=value
    ---> Running in 553e18f8b3ad
    Removing intermediate container 553e18f8b3ad
    ---> 34a658bf6255
    Step 3/4 : RUN apk add --quiet py3-pip  && pip install --quiet cowsay
    ---> Running in d849133b6473
    WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv
    Removing intermediate container d849133b6473
    ---> b5db91e1c97d
    Step 4/4 : CMD ["cowsay", "-t", "Hello World"]
    ---> Running in 98ad5bf0e85e
    Removing intermediate container 98ad5bf0e85e
    ---> d4d61ab0346e
    Successfully built d4d61ab0346e
    Successfully tagged test:latest
    ```

=== "With BuildKit"
    ```{ .bash .copy }
    DOCKER_BUILDKIT=1 docker build -t test --no-cache .
    ```

    The output does not show all the verbose command output and is meant to be easer to read. BuildKit also provides more functionality than the legacy builder.

    ```  { .bash .no-copy }
    $ DOCKER_BUILDKIT=1 docker build -t test --no-cache .
    #0 building with "desktop-linux" instance using docker driver

    #1 [internal] load .dockerignore
    #1 transferring context: 2B done
    #1 DONE 0.0s

    #2 [internal] load build definition from Dockerfile
    #2 transferring dockerfile: 195B done
    #2 DONE 0.0s

    #3 [internal] load metadata for docker.io/library/alpine:3.18.0
    #3 DONE 0.0s

    #4 [1/2] FROM docker.io/library/alpine:3.18.0
    #4 CACHED

    #5 [2/2] RUN apk add --quiet py3-pip  && pip install --quiet cowsay
    #5 2.976 WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv
    #5 DONE 3.3s

    #6 exporting to image
    #6 exporting layers
    #6 exporting layers 0.2s done
    #6 writing image sha256:808026cb4724dad11bfc93e48709f10a8a0ea2741db934d00abca322d22270a3 done
    #6 naming to docker.io/library/test done
    #6 DONE 0.2s
    ```
