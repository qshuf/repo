# Building Containers

Building containers is important to understand the inner workings and peculiarities of the container ecosystem.

This chapter walks you through how containers are build and provides best-practices on how to build it.
