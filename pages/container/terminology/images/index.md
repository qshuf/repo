# Container Image

A container image is an artefact that bundles a file-system and the initial (default) configuration of a container. It is a read-only template that is used to create a container.

The most common format is either Docker (v2) or the OCI image format. The OCI format was created from teh Docker format and is now the standard for container images. Thus, they are only differ in nuances and the average user won't be able to tell the difference. But for the sake of staying consistent we are going to use the OCI format thoughout this workshop.

## Manifest

The container image boils down to a lot of JSON and a bunch of content addressed tar files. The JSON is called the **manifest** and contains all the information about the image.

![Manifest with two layers](./manifest-layers.png)

A manifest states the configuration object (itself a JSON blob) and the layers that constitute the file-system of the container. The layers are ordered and the first layer is the base layer. Each layer is a tar file that is unpacked on top of the previous layer.

### Configuration Object

The configuration object describes the default configuration of a container. It contains the entrypoint, the command, the environment variables, the working directory, and so on.
When a container is actually created the configuration object is used to create a container runtime configuration, which is picked up by the container runtime.

### Layer

A layer is a tar file that is unpacked on top of the previous layer. It contains the file-system changes that are applied to the previous layer. The first layer is called the base layer and is usually an operating system.

## Distribution aspect

(OCI/Docker) Container Images are optimized for distribution and effecient storage of images. Every aspect is a content-addressable hash and the container image is itself a collection of hashes (manifest, configuration, layers). This allows to store the image in a registry and only pull the layers that are missing.
By splitting container-filesystems into smaller layers the download can be parallelized and the storage can be deduplicated. This is especially useful when you have multiple images that share the same base layer(s).

## Snapshots

When a container is created the container engine unpacks all layers of a container image into a directory (it most likely uses an overlay file-system). This ready-to-be-started artefact is called **Snapshot**.

!!! warning "Snapshot vs Image"

    Some confuse image and snapshot as the same thing. Singularity uses the same format for distribution and execution. Thus, the image is the snapshot. But in the container ecosystem the image is the read-only template and the snapshot is the unpacked file-system ready to be started.
