# Container Tech Terminology

This page is a collection of terms used in the container tech ecosystem to help you understand the concepts.

## General Terms

From a high level perspective a container is a process running on a host. The process is isolated by using namespaces and constraint by CGroups.

- **Namespaces**: Linux kernel feature to isolate the resources of different processes (/container)
- **CGroups**: Linux kernel feature to constraint the resources of a process. (throttle IO/network bandwith, limit memory, et cetera).
- **Container**: A process (tree) running on a host, isolated by namespaces and constraint by CGroups.
- **Container Runtime**: The software that starts a container. It uses the container runtime interface to do so.
- **Container Engine**: The software that manages containers (pull images, configure networks, etc.). It uses the container runtime interface to do so.
- **OCI**: Open Container Initiative. A project to standardize container images and runtimes.
