# Namespaces

A namespace is used by the linux kernel to track resource use of processes. Without container a host has only one namespace for each resource.  The initial namespaces are as follows:

1. **PID**: Process ID Namespace isolates the view of the process tree.
2. **MNT**: Mount Namespace isolates the view of the file-system.
3. **NET**: Network Namespace isolates the view of the network stack, routing, IP addresses and such.
4. **IPC**: Inter-Process Communication Namespace isolates the view of the System V IPC and POSIX message queues. Communication via shared memory is isolated by this.
5. **UTS**: Unix Timeshare Namespace. A historical namespace which isolates the hostname and domain name.

Over time, more namespaces are added. The most important one might be the **USER** namespace. It allows to map a user inside of a container to a different user outside. A prime example is a container that uses a privileged user inside the container (like nginx) but maps it to a non-privileged user outside.

A kind of weird one is the **TIME** namespace. It allows to set the time for a process. This way you can run a process in a different timezone than the host - but there are going to be a lot of side effects.

=== "Namespaces"

    ``` { .bash .no-copy }
    $ lsns
            NS TYPE   NPROCS   PID USER             COMMAND
    4026531834 time      141     1 root             /sbin/init
    4026531835 cgroup    141     1 root             /sbin/init
    4026531836 pid       141     1 root             /sbin/init
    4026531837 user      141     1 root             /sbin/init
    4026531838 uts       138     1 root             /sbin/init
    4026531839 ipc       141     1 root             /sbin/init
    4026531840 net       141     1 root             /sbin/init
    4026531841 mnt       135     1 root             /sbin/init
    ```
    A container on the other hand only sees its own processes when started with a PID namespace (which is the case by default).

    ``` { .bash .no-copy }
    localhost ~$ unshare --pid --map-root-user --mount-proc --fork bash
    ~# ps -ef
    UID          PID    PPID  C STIME TTY          TIME CMD
    root           1       0  0 11:56 pts/3    00:00:00 bash
    root           4       1  0 11:56 pts/3    00:00:00 ps -ef
    ```

=== "PID Namespace"

    One of the easiest to understand namespace is the PID namespace. You can see all the processes of a namespace using `ps -ef`. 
    The first Process ID (PID) is always 1 and is called the init process. It is the parent of all other processes in the namespace.
    Ok, **almost** all - `kthreadd` is typically used to manage hardware.

    ``` {.bash .no-copy}
    $ ps -ef
    UID          PID    PPID  C STIME TTY          TIME CMD
    root           1       0  0 11:16 ?        00:00:02 /sbin/init
    root           2       0  0 11:16 ?        00:00:00 [kthreadd]
    root           3       2  0 11:16 ?        00:00:00 [rcu_gp]
    root           4       2  0 11:16 ?        00:00:00 [rcu_par_gp]
    ```
    
    Within a namespace, you'll only see the processes of that namespace. We forked `bash` into a new PID namespace; which will be the PID 1 process with no parent.

    ``` { .bash .no-copy }
    localhost ~$ unshare --pid --map-root-user --mount-proc --fork bash
    ~# ps -ef
    UID          PID    PPID  C STIME TTY          TIME CMD
    root           1       0  0 11:56 pts/3    00:00:00 bash
    root           4       1  0 11:56 pts/3    00:00:00 ps -ef
    ```

=== "MNT Namespace"

    The mnt namespace is also rather old and easy to understand. It isolates the file-system view for a process. Running an Alpine container we only have access to the alpine root file-system.

    ``` {.bash .no-copy}
    $ docker run -ti --rm alpine:3.18.0 cat /etc/os-release
    NAME="Alpine Linux"
    ID=alpine
    VERSION_ID=3.18.0
    PRETTY_NAME="Alpine Linux v3.18"
    HOME_URL="https://alpinelinux.org/"
    BUG_REPORT_URL="https://gitlab.alpinelinux.org/alpine/aports/-/issues"
    ```
