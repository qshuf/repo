# Install Docker Desktop on Windows

The process is similar to the Mac install - head over to [Docker.com/get-started](https://www.docker.com/get-started/) and download the installer.

Afterwards you just start the Docker application and off you go.

## Hello World

To test the installation you might want to open a terminal (`cmd` or `powershell`) and run the following command.

!!! warning

    Since I do not have a windows system at hand, I copied the UNIX (mac/linux) example. That might look different in Windwos.

``` { .bash .copy }
docker run -ti --rm alpine:latest echo Hello World
```

We are going to dive deeper into what is going on; for the time being we are all good if you see the following output:

``` { .bash .no-copy }
$ docker run -ti --rm alpine:latest echo Hello World
Hello World
```
