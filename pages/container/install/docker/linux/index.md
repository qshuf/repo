# Install Docker on Linux

The guidance presented here is going to focus on the plain [docker engine](https://docs.docker.com/engine/). Some distributions like Ubuntu provide their own `docker` package which might have different behaviors.

If you are a brave soul, you might just try to install the default `docker` package. Please provide feedback on how that works for you. :)

=== "Debian"

    Detailed install instruction can be found on the [docker website](https://docs.docker.com/engine/install/debian/). The gist of it is.

    0. Run the following command to uninstall all conflicting packages:

    ``` bash
    for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done
    ```
    
    1. Update the `apt` package index and install packages to allow `apt` to use a repository over HTTPS:

    ```bash
    sudo apt-get update
    sudo apt-get install ca-certificates curl gnupg
    ```
    
    2. Add Docker’s official GPG key:

    ```bash
    sudo install -m 0755 -d /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    sudo chmod a+r /etc/apt/keyrings/docker.gpg
    ```

    3. Use the following command to set up the repository:

    ```bash
    echo \
        "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
        "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
        sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    ```

    4. Update the apt package index:

    ```bash
    sudo apt-get update
    ```

    5. Install the latest version

    ```bash
    sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    ```

=== "CentOS/RHEL/"

    ``` bash
    something else
    ```

## Hello World

To test the installation you might want to open a terminal (`Terminal` or `iTerm`) and run the following command.

``` { .bash .copy }
docker run -ti --rm alpine:latest echo Hello World
```

We are going to dive deeper into what is going on; for the time being we are all good if you see the following output:

``` { .bash .no-copy }
$ docker run -ti --rm alpine:latest echo Hello World
Hello World
```
