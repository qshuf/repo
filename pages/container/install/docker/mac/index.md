# Install Docker Desktop on Mac

The process is pretty straight forward - head over to [Docker.com/get-started](https://www.docker.com/get-started/) and download the installer for your platform (Intel/ARM).

Afterwards you just start the Docker application and off you go.

## Hello World

To test the installation you might want to open a terminal (`Terminal` or `iTerm`) and run the following command.

``` { .bash .copy }
docker run -ti --rm alpine:latest echo Hello World
```

We are going to dive deeper into what is going on; for the time being we are all good if you see the following output:

``` { .bash .no-copy }
$ docker run -ti --rm alpine:latest echo Hello World
Hello World
```
