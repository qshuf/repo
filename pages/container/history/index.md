# Histroy of Container

The actual root of this tech dates back to 1979 and the advent of chroot in Unix v7.


But let's start with the assumption that a container isolates a process on-top of the Linux kernel. Using namespaces, eventually cgroups.

## The early days

cgroups were added to the Linux kernel in 2007 (work started in 2006 under the name *process containers*) and with that a couple of projects adopted it:

1. [LXC](https://linuxcontainers.org/lxc/): *LinuX Containers* where the first to use chroot, cgroups and namespaces to create a container. Even tho it still felt like a virtual machine, the library became the basis for the docker project.
1. [LMCTFY](https://github.com/google/lmctfy): *Let Me Containerize That For You* was a project by Google to create a container runtime. The project team collaborated with the docker team back in the days and 'donated' the core concepts to [libcontainer](https://pkg.go.dev/github.com/opencontainers/runc/libcontainer) and thus became the bases of [runc](https://pkg.go.dev/github.com/opencontainers/runc).
1. [systemd-nspawn](https://www.freedesktop.org/software/systemd/man/systemd-nspawn.html): This project allowed processes to be started as a container. It was part of the systemd project and is still used today to start containers.
1. [rkt](https://github.com/rkt/rkt): *Rocket* was a container runtime by CoreOS. It was a nimble little tool with a focus to be CLI driven and it was daemonless.

It uses container tech like cgroups but its goal is a 'system-container'. Which means you need to define network interfaces, virtual disks, and so on.
It feels like a virtual machine rather than a light weight isolated process. That said, `docker` used the lxc libraries to begin with, so a lot was already done and we are all greatful. But I won't go back. :)

The rest popped up along the way, but with docker gaining momentum and popularity they were more or less abadoned eventually.

## Break up the stack

Docker back then was a single binary doing everything from being the command line interface the user interacted with to creating a cluster of nodes using Docker Swarm.

When the container ecosystem evolved folks were not pleased to run the docker binary just to plug into a scheduler like Kubernetes. Eventually the container runtime interface was formed and Docker Inc broke up the monolith into `runc` (Runtime), `containerd` (Container Runtime Interface), `dockerd` (Engine), and even `swarmkit` (Scheduler).

`runC` became the first official runtime (as said, donated by Docker Inc). Likewise `containerd` was the first official container runtime interface.

### Runtimes

We'll dive deeper into runtimes later. There are two categories:

1. **Native**: runtimes using the same kernel as the operating system
1. **Sandboxed** or **virtualised**: runtimes using a different kernel, either in some kind of virtual machine or in a sandbox like [gvisor](https://gvisor.dev/) (against what they call an *application kernel*).

### Container Runtime Interface

As said, `containerd` is the first official container runtime interface. It takes care of the local lifecycle and configuration of a container to hand the execution of to some other runtime.
The Kubernetes ecosystem recently abadoned `containerd` and started using `cri-o` instead.

## Onwards and upwards

That should provide enough historical context to get us started. Kubernetes uses the runtime interface to start containers and the HPC ecosystem has come up with ideas which deserve a second page (TBD).
