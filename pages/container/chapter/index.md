# Container Technology

Encapsulation and abstraction of computer systems is nothing new. From big old mainframes which where able to run applications in total application to lightweight containers we did come a long way. :) 

The term container nowadays refers to the use of Linux Kernel features like namespaces and Control Groups (CGroups) and seccomp filter.

Namespaces isolate process resources while CGroups and seccomp filter contrain what the application can do against the kernel.
