# VMs vs Containers

## Virtual Machines

VMs are a way to run multiple operating systems on a single machine. They create virtual hardware interfaces so that a guest operating systems can interface with virtual hardware. This allows you to run multiple operating systems on a single machine.

=== "Kernel"

    ![](kernel_user.png)

    On-top of the real hardware a base operating system is installed. To interface with the real hardware a kernel provides a stable API. A userland is then installed on-top of the kernel. Depending on which OS is used the userland can be different. For example, the userland of a Fedora system is different from the userland of a Debian based OS.

=== "Hypervisor"

    ![](vms.png)
    
    To create different guests ontop of a single machine a hypervisor is used. The hypervisor creates virtual hardware interfaces for each guest. The guest operating system then interfaces with the virtual hardware. The hypervisor then translates the virtual hardware calls to the real hardware.

## Containers

Virtualization using container is also refered to as kernel level virtualization. Instead of create a complete set of virtual hardware devices and spinning up a complete operating system, containers use the host kernel and isolation features to create a virtual environment for a process.

=== "Kernel"

    ![](kernel_cnt.png)

    For run containerized processes no additional hypervisor needs to be installed - it's already included in the Linux Kernel. To get a more convinient experience container engines like Docker might be used, but they are not required. They abstract away all the nitty-gritty details of what is going on.

=== "Container"

    ![](cnt.png)

    A containerized process is a process that is isolated from the host system. It can only see the processes and files that are within the container. The containerized process is still running on the host kernel, but it is isolated from the host system and other container instances.
