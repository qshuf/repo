# No Tools Image build

As dicussed in other chapters container images are 'just' a bunch of tar-balls (which define the contianer filesystem to start) and a JSON blob that provide the default runtime configuration and some metadata.

This [blog post](https://ravichaganti.com/blog/2022-11-28-building-container-images-using-no-tools/) by Ravikanth Chaganti describes the steps to create a container image without the use of special tools.  
We'll mimic the steps here, but instead of using a public registry, we are boiling it down to the bare minimum and use a local registry (w/o authentication).

## Download the file-system

First, we'll download an alpine file-system which is conveniently provided as a tar-ball of the right format.

```{ .bash .copy}
export URL=https://dl-cdn.alpinelinux.org/alpine/
wget -q \
   ${URL}/v3.18/releases/x86_64/alpine-minirootfs-3.18.3-x86_64.tar.gz \
   -O alpine.tar.gz
```

## Create the configuration

Next, we'll create a very simple container configuration to start with.

```{ .bash .copy}
cat > config.json << EOF
{
    "architecture": "amd64",
    "os": "linux",
    "config": {
      "Env": [
        "TERM=xterm",
        "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
      ],
      "Entrypoint": [
        "ash"
      ],
      "Cmd": [
        "echo Hello Alpine"
        ]
    },
EOF
```

To add the tar-ball as a layer to our image, we need to generate a hashsum.  
We'll use the `gunzip` command to decompress the tar-ball and pipe it into the `sha256sum` command to generate the hashsum of the tar-ball, not the (g)uncompressed tar-ball. This makes sure that a configuration does not change only because the tar-ball is compressed differently.

=== "Linux"

    ```{ .bash .copy}
    export LSHA=$(gunzip < alpine.tar.gz | sha256sum - |awk '{print $1}')
    ```

=== "MacOS"

    ```{ .bash .copy}
    export LSHA=$(gunzip < alpine.tar.gz | shasum -a 256 - |awk '{print $1}')
    ```

```{ .bash .no-copy }
$ gunzip < alpine.tar.gz | sha256sum
505546e2cf31752fe29a5278c71e815da174aab3f18de1192b0231900f45b3b9  -
```

We can now add the tar-ball to our image.

```{ .bash .copy}
cat >> config.json << EOF
  "rootfs": {
    "type": "layers",
    "diff_ids": [
      "sha256:${LSHA}"
    ]
  },
  "history": [{}]
}
EOF
```

## Create the manifest

Now we have both, the configuration and the file-system, we can prepare for the manifest to be build.
We need a hashsum of the configuration and the file-system.

=== "Linux"

    ```{ .bash .copy}
    export CFGSHA=$(sha256sum < config.json |awk '{print $1}')
    export L0SHA=$(sha256sum < alpine.tar.gz |awk '{print $1}') # (1)!
    ```

    1. For the manifest we are using the hashsum of the compressed layer. The manifest does care which compress algorithm is used.

=== "MacOS"

    ```{ .bash .copy}
    export CFGSHA=$(shasum -a 256 < config.json |awk '{print $1}')
    export L0SHA=$(shasum -a 256 < alpine.tar.gz |awk '{print $1}')
    ```

Let's create the manifest.

```{ .bash .copy }
cat > index.json << EOF
{
  "schemaVersion": 2,
  "mediaType": "application/vnd.oci.image.manifest.v1+json",
  "config": {
    "mediaType": "application/vnd.oci.image.config.v1+json",
    "size": 277,
    "digest": "sha256:${CFGSHA}"
  },
  "layers": [
    {
      "mediaType": "application/vnd.oci.image.layer.v1.tar+gzip",
      "size": 1074934,
      "digest": "sha256:${L0SHA}"
    }
  ]
}
EOF
```

## Upload the bits'n'pieces

To make use of the image we just created, we need to upload the bits'n'pieces to a registry.
For that, we'll start a local registry:

```{ .bash .copy}
docker run --platform=linux/amd64  -ti --rm -p 5050:5050  -e REGISTRY_HTTP_ADDR=0.0.0.0:5050 registry:2.8.1
```

We need to upload the layer, the config and the manifest itself. For that, please open another terminal window (/tab).

### Upload the `config.json`

```{ .bash .copy}
curl -sX POST --head "http://localhost:5050/v2/alpine/blobs/uploads/" |tee cfg-upload.log
cat cfg-upload.log |awk '/^Location/{print $2}' |sed 's/%3D/=/g'
```

Please copy&paste the long URL from the output of the last command into the next command.

```{ .bash .copy}
export CFGLOC=<paste the long URL here>
```

Ok, now we can upload the config.

```{ .bash .copy}
curl -X PUT \
    -H "Content-Type: application/octet-stream" \
    "${CFGLOC}&digest=sha256:${CFGSHA}" \
     --data-binary @config.json
```

The registry will respond with a `201 Accepted` in the logs of the container.

```{ .bash .no-copy}
$ docker logs -n1 $(docker ps -ql)
172.17.0.1 - - [14/Sep/2023:08:33:43 +0000] "POST /v2/alpine/blobs/uploads/ HTTP/1.1" 202 0 "" "curl/8.1.2"
INFO[0453] response completed                            go.version=go1.16.15 http.request.contenttype="application/octet-stream" http.request.host="localhost:5050" http.request.id=bf1267be-40d6-42d8-a865-1272d84f8c79 http.request.method=PUT http.request.remoteaddr="172.17.0.1:41556" http.request.uri="<snip>" http.request.useragent="curl/8.1.2" http.response.duration=46.13775ms http.response.status=201 http.response.written=0
172.17.0.1 - - [14/Sep/2023:08:34:08 +0000] "PUT /v2/alpine/blobs/uploads/<snip> HTTP/1.1" 201 0 "" "curl/8.1.2"
```

And we can find the data in the container itself.

=== "Cmd"

    ```{ .bash .copy}
    docker exec -ti $(docker ps -ql) find /var/lib/registry -type f
    ```

=== "Output"

    ```{ .bash .no-copy}
    $ docker exec -ti $(docker ps -ql) find /var/lib/registry -type f
    /var/lib/registry/docker/registry/v2/repositories/alpine/_layers/sha256/cec4d49438ac7627ba71c0ebea57c719fafd29453726aba1742d53264eb37cfb/link
    /var/lib/registry/docker/registry/v2/blobs/sha256/ce/cec4d49438ac7627ba71c0ebea57c719fafd29453726aba1742d53264eb37cfb/data
    ```

### Upload the layer

A layer is a blob like the config was. Thus, we are using the same process to upload the layer.

```{ .bash .copy}
curl -sX POST --head "http://localhost:5050/v2/alpine/blobs/uploads/" |tee lyr-upload.log
cat lyr-upload.log |awk '/^Location/{print $2}' |sed 's/%3D/=/g'
```

Please copy&paste the long URL from the output of the last command into the next command.

```{ .bash .copy}
export LYRLOC=<paste the long URL here>
```

Ok, now we can upload the config.

```{ .bash .copy}
curl -X PUT \
    -H "Content-Type: application/octet-stream" \
    "${LYRLOC}&digest=sha256:${L0SHA}" \
     --data-binary @alpine.tar.gz
```

Ok, onwards with the upload. We'll upload the config and the manifest.

```{ .bash .copy}
export MANIFESTSHA=$(shasum -a 256 < index.json |awk '{print $1}')
curl -X PUT \
    -H "Content-Type: application/vnd.oci.image.manifest.v1+json" \
    "http://localhost:5050/v2/alpine/manifests/latest" \
    --data-binary @index.json
```

## Pulling the new Image

Et voila! We can now pull the image.

=== "Command"

    ```{ .bash .copy}
    docker pull localhost:5050/alpine:latest
    ```

=== "Output"

    ```{ .bash .no-copy}
    $ docker pull localhost:5050/alpine:latest
    latest: Pulling from alpine
    fc577324b7e9: Pull complete
    Digest: sha256:7a8d6ea0334cef2d7af2c0e86538e5a1d499058c3f9ff8929735ec3eae80e685
    Status: Downloaded newer image for localhost:5050/alpine:latest
    localhost:5050/alpine:latest
    ```
