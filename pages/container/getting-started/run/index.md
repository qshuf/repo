---
title: Run a Container
hide:
 - toc
weight: 1
---

Docker is using a client-server model. You run a command like `docker ps` and the server will respond.

Why? Docker is supposed to start non-interactive containers - you start a container, close your terminal and the container is still running. For containers running services like a web server that makes total sense. And since Docker is focused on the average user - a client-server model is totally fine. Back in the days (way back) the client and the server were even the same binary.

## Hello World

To run a container that prints out `Hello World` we use the following command.

``` { .bash .copy }
docker run -ti \
            --rm \
            alpine:latest \
            echo Hello World
```

This will instanciate a container and execute `echo Hello World` within the isolated container.

``` { .bash .no-copy }
$ docker run -ti \ # (1)!
            --rm \ # (2)!
            alpine:latest \
            echo Hello World
Hello World
```

1. `-t` (`--tty`) captures the output (stderr/stdout) using a pseudo-TTY. In simple terms it attaches a terminal to represent the output.
    `-i` (`--input`) attaches stdin, so that you can actually type or pipe input into the container.
2. `--rm` is going to remove the container from the system once it finshes.

## Diving Deeper

What actually happens when you run `docker run` are three things.

### 1. Pull the image

The engine will first check whether it already has an image `alpine:latest` for your architecture in stock. If it does (and it does not check for a newer one btw) it will move on to the next step; otherwise it will pull the image.

``` { .bash .copy }
docker pull alpine:3.18.0
```

``` {.bash .no-copy}
$ docker pull alpine:3.18.0
3.18.0: Pulling from library/alpine # (1)!
Digest: sha256:02bb6f428431fbc2809c5d1b41eab5a68350194fb508869a33cb1af4444c9b11 # (2)!
Status: Image is up to date for alpine:3.18.0 # (3)!
docker.io/library/alpine:3.18.0
```

1. The docker deamon is going to pull the manifest of the image. The manifest is a json file that contains all the information about the image; the default startup configuration, file-system layers, et cetrea.
2. Docker will then check if all the layers are already present in the local cache. If not, it will pull the layers from the registry.
3. Since all layers are already cached, no need for extra work.

### 2. Create a container

The image pulled from the registry is 'just' a bunch of json and tar files to constitute a file-system the container can use. To actually run a container you need a file-system directory the container have exclusive access to. Thus, docker is going to create a container instance without actually starting it (just prepare everyting).

``` { .bash .copy }
docker create -t \
    alpine:3.18.0 \
    echo Hello World
```

``` { .bash .annotate .no-copy }
$ docker create -t \ #(1)
    alpine:3.18.0 \
    echo Hello World #(2)
80a9591b1b4b68a22eac11c880faddd69c8e8f5c886f468499eb7a484bd53c16
```

1. `--tty` *allocates a pseudo-tty*. Which means it captures stdout and stderr messages from the executing process. :)
2. The part behind the image name is called the command (CMD) and will be appended to the entrypoint. As the entrypoint usually is a shell (`/bin/bash` or `/bin/sh`) a container might expect shell commands. Some containers specificing an application as entrypoint (`skopeo`), which lets the container act as a standin for the application itself.

### 3. Start the container

The `docker create` command will output a hash - the hash of the instantiated container. 

``` { .bash .copy }
docker start -a <hash>
```

``` {.bash .no-copy}
docker start -a \ #(1)!
    80a9591b1b4b68a22eac11c880faddd69c8e8f5c886f468499eb7a484bd53c16 #(2)!
Hello World
```

1. `-a` (`--attach`) attaches the output of the container to the current terminal.
2. The container ID is copied from the output of the `docker create` command.

## As Single Block

You can also run all three commands in a single block.

``` { .bash .copy }
docker pull -q \
    alpine:3.18.0
CNT_ID=$(docker create -t alpine:3.18.0 echo Hello World)
docker start -a $CNT_ID
```

``` {.bash .no-copy}
> docker pull -q \ #(1)!
    alpine:3.18.0
> CNT_ID=$(docker create -t alpine:3.18.0 echo Hello World)
> docker start -a $CNT_ID
docker.io/library/alpine:3.18.0 #(2)!
Hello World #(3)!
```

1. `-q` (`--quiet`) suppresses the output of the `docker pull` command. Otherwise it is pretty noisy.
2. The `docker pull` command will output the image name and tag.
3. The `docker start` command will output the output of the container (since we attached using `-a`).

## Containers Restart

So far we started containers without removing them afterwards. This allows us to restart a container as often as we like. 

``` {.bash .copy}
docker create -t --name test \
    alpine:3.18.0 date
docker start -a test && sleep 2
docker start -a test && sleep 2
docker start -a test && sleep 2
docker rm test
```

``` {.bash .no-copy}
> docker create -t --name test \ #(1)!
    alpine:3.18.0 date
> docker start -a test && sleep 2 #(2)!
> docker start -a test && sleep 2
> docker start -a test && sleep 2
> docker rm test # (3)!
7bd5ade32211c92602fe1acc066b27a91cd614a9d9bb3f739df4ab68a1be8350
Tue Aug 29 08:52:54 UTC 2023
Tue Aug 29 08:52:57 UTC 2023
Tue Aug 29 08:52:59 UTC 2023
```

1. We create a container with the name `test` and run the `date` command. Instead of a random name (adjective_noun) we can specify a name ourselves. This allows us to reference the container by name instead of ID.
2. We start the container three times and wait 2 seconds afterwards.
3. We remove the container from the system after we are done. Otherwise the name `test` will be taken and we can't create a new container with that name. Naming containers is going to hurt you in the long run. Try to avoid it. :smile:

!!! success "lightweight containers"
    Let's pause for a minute and reflect how the restart exposes a difference to traditional virtualization. `docker start` does not spin up a bunch of virtualized hardware and spawns a kernel to run a simple command - it just forks a process into the same prepared container instance.

### Deeper dive into restart

Let's take a deeper dive into the restart process. We'll create a container that writes the current date into a file and restart it twice.

``` {.bash .copy}
cat > entry.sh << \EOF
#!/bin/ash

echo "## Add entry to ./out.txt in '$(pwd)' within the container"
date |tee -a out.txt
echo "## Content of ./out.txt"
cat out.txt
EOF
chmod +x entry.sh
CNT_ID=$(docker create -t \
    -v $(pwd):/data \
    -w /opt \
    alpine:3.18.0 \
    /data/entry.sh)
docker start -a ${CNT_ID} && sleep 1
docker start -a ${CNT_ID} && sleep 1
docker rm ${CNT_ID}
```

Ok, let's unpack what is going on here. First the preperation of the script we are going to run within the container.  
We are using a [here-document](https://en.wikipedia.org/wiki/Here_document) (`cat > entry.sh << \EOF`) to create a script called `entry.sh`. 
The script will write the current date into a file called `out.txt` and print the content of the file.

``` {.bash .no-copy}
#!/bin/ash

echo "## Add entry to ./out.txt in '$(pwd)' within the container"
date |tee -a out.txt #(1)!
echo "## Content of ./out.txt"
cat out.txt
```

1. We are using `tee` to append the output of `date` into the file and print it to stdout.

Now the actual container lifecycle commands.

``` {.bash .no-copy}
CNT_ID=$(docker create -t \
    -v $(pwd):/data \ #(1)!
    -w /opt \ #(2)!
    alpine:3.18.0 \
        /data/entry.sh)
docker start -a ${CNT_ID} && sleep 1
docker start -a ${CNT_ID} && sleep 1
docker rm ${CNT_ID}
## Add entry to ./out.txt in '/opt' within the container
Tue Aug 29 09:06:38 UTC 2023
## Content of ./out.txt
Tue Aug 29 09:06:38 UTC 2023
## Add entry to ./out.txt in '/opt' within the container
Tue Aug 29 09:06:39 UTC 2023
## Content of ./out.txt
Tue Aug 29 09:06:38 UTC 2023
Tue Aug 29 09:06:39 UTC 2023
98242353ba2ca7ddd95354d4e7e75379cfed5ae2627d6b14a15b615932761c3c
```

1. we are mounting the current directory into the container under `/data`, so that we are able to execute `/data/entry.sh` within the contianer which is the script we created above.
2. we are chaging the working directory of the container to `/opt` so that the output file is written to `/opt/out.txt` (which would be the current directory).

!!! success "persistent container file-system"
    As you can see from the output of the second run, the container process is restarted - but the container file-system is persistent across multiple restarts. This is why containers are so lightweight and fast to start.
