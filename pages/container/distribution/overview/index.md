# Container Distribution

The exchange of containers is usually done via a container registry. These are optimized to store and distribute the containers efficiently. The most popular container registry is [Docker Hub](https://hub.docker.com/). It is a public registry, which means that everyone can pull and push images to it. There are also private registries, which are usually hosted by companies themselves. The most popular private registry is [Quay](https://quay.io/).

