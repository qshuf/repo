# Bibliotheken

Wir haben schon ohne es anzusprechen Bibliotheken benutzt. In GOLANG sind Bibliotheken ein wichtiger Bestandteil. Sie sind in Paketen organisiert und können in anderen Programmen wiederverwendet werden. In diesem Kapitel werden wir uns einige der wichtigsten Bibliotheken ansehen.

Bibliotheken in GOLANG nennen sich `packages`, daher werden wir sie auch so nennen.

## Standardpackete

GOLANG wird mit einer großen Anzahl von Standardpaketen geliefert, die in der [Dokumentation](https://golang.org/pkg/) aufgelistet sind. Einige der wichtigsten sind:

- `fmt` - Formatierung von Text
- `os` - Betriebssystemfunktionen
- `io` - Ein- und Ausgabe
- `net/http` - HTTP-Client und -Server
- `encoding/json` - JSON-Serialisierung
- `testing` - Testen von Code
- `time` - Zeit- und Datumsfunktionen

Um diese zu nutzen, müssen wir sie importieren. Zum Beispiel:

```go
package main
import "fmt"
func main() {
    fmt.Println("Hello, World!")
}
```

In diesem Beispiel importieren wir das `fmt`-Paket und verwenden die Funktion `Println` um "Hello, World!" auf der Konsole auszugeben.

## Externe Pakete

Neben den Standardpaketen gibt es eine große Anzahl von externen Paketen, die von der Community entwickelt wurden. Diese können mit dem Befehl `go get` installiert werden. Zum Beispiel:

```bash
go mod init example.com/hello
go get github.com/gorilla/mux
```

Dieser Befehl installiert das Paket `gorilla/mux` in deinem GOPATH.
Im Anschluss können wir das Paket importieren und verwenden:

```go
cat > main.go << EOF
package main
import (
    "net/http"
    "github.com/gorilla/mux"
)

func HomeHandler(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte("Hello, World!"))
}
func main() {
    r := mux.NewRouter()
    r.HandleFunc("/", HomeHandler)
    http.Handle("/", r)
    http.ListenAndServe(":8080", nil)
}
EOF
go run main.go
```

In diesem Beispiel verwenden wir das Paket `gorilla/mux` um einen einfachen HTTP-Server zu erstellen. Der Server gibt "Hello, World!" zurück, wenn er aufgerufen wird.

``` { .bash .no-copy}
$ curl http://localhost:8080
Hello, World!%
```
