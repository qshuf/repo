# Defer

Innerhalb von Funktionen werden Anweisungen in der Reihenfolge ausgeführt, in der sie geschrieben wurden. Mit `defer` kannst Du eine Funktion oder Methode aufrufen, die erst nach dem Abschluss der umgebenden Funktion ausgeführt wird.

So kann man z.B. geöffnete Dateien schließen, Datenbankverbindungen schließen oder temporäre Dateien löschen.

```go
cat > main.go << EOF
package main

func A() {
    defer B()
    println("A")
}

func B() {
    println("B")
}

func C() {
    println("C")
}
func main() {
    A()
    C()
}
EOF
go run main.go
```

```{ .bash .no-copy }
$ go run main.go
A # `A` wird zuerst aufgerufen - keine Überraschung
B # `B` wird erst aufgerufen, wenn `A` fertig ist
C # `C` wird als nächstes aufgerufen
```
