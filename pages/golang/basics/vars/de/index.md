# Variablen

In Go gibt es verschiedene Arten von Variablen. In diesem Abschnitt werden wir uns die verschiedenen Arten von Variablen ansehen und wie sie deklariert und initialisiert werden.

## Variablendeklaration

Variablen in Go werden mit dem `var`-Schlüsselwort deklariert. Der Syntax für die Deklaration einer Variablen lautet:

```go
var name type
```

Hier ist ein Beispiel:

```go
var x int
```

In diesem Beispiel wird eine Variable `x` vom Typ `int` deklariert.

## Variableninitialisierung

Variablen in Go können auch bei der Deklaration initialisiert werden. Der Syntax für die Initialisierung einer Variablen lautet:

```go
var name type = value
```

Hier ist ein Beispiel:

```go
var x int = 5
```

In diesem Beispiel wird die Variable `x` vom Typ `int` mit dem Wert `5` initialisiert.

## Kurzschreibweise

Go bietet auch eine Kurzschreibweise für die Deklaration und Initialisierung von Variablen. Der Syntax für die Kurzschreibweise lautet:

```go
name := value
```

Hier ist ein Beispiel:

```go
x := 5
```

In diesem Beispiel wird die Variable `x` mit dem Wert `5` initialisiert. Der Typ der Variable wird automatisch vom Compiler abgeleitet.

## Mehrere Variablen

Go ermöglicht auch die Deklaration und Initialisierung mehrerer Variablen in einer Zeile. Der Syntax für die Deklaration und Initialisierung mehrerer Variablen lautet:

```go
var name1, name2 string = "value1", "value2"
```

