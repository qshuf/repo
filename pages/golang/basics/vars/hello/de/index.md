# Hello Vars

Variablen können nur einmal deklariert oder initialisiert werden.

``` go linenums="1" hl_lines="7-8"
package main

import "fmt"

func main() {
    var x int
    x := 5
    fmt.Println(x)
}
```

Die beiden Zeilen `x := 5` und `var x int` sind nicht zulässig, da die Variable `x` bereits deklariert wurde.

![Error](fail1.gif)

## Scope

Wie in vielen anderen Sprachen haben Variablen in Go einen Gültigkeitsbereich, der als _Scope_ bezeichnet wird. Der Scope einer Variablen bestimmt, wo sie im Code sichtbar ist und wo sie verwendet werden kann.

``` go linenums="1" hl_lines="6 8"
package main

import "fmt"

func main() {
    x := 5
    if true {
        x := 10
        fmt.Println(x)
    }
    fmt.Println(x)
}
```

Variablen können im selben Scope nicht erneut deklariert werden, aber sie können _überschattet_ werden. Das bedeutet, dass eine Variable mit demselben Namen wie eine äußere Variable im selben Scope deklariert werden kann.

Hier wird die Variable `x` innerhalb der `if`-Anweisung neu deklariert. Diese neue Variable `x` hat jedoch nur Gültigkeit innerhalb des `if`-Blocks. Die ursprüngliche Variable `x` bleibt unverändert.

![Scope](scope.gif)

Verändert man zeile 10 zu `x = 10`, wird die ursprüngliche Variable `x` überschrieben.

``` go linenums="1" hl_lines="8"
package main

import "fmt"

func main() {
    x := 5
    if true {
        x = 10
        fmt.Println(x)
    }
    fmt.Println(x)
}
```

![Scope](scope2.gif)
