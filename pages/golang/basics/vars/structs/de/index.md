# Structs

Variablen können auch in einer Struktur zusammengefasst werden. Eine Struktur ist eine Sammlung von Feldern.

```go
package main

import "fmt"

type person struct {
    name string
    age  int
}

func main() {
    p := person{name: "Alice", age: 30}
    fmt.Println(p)
    fmt.Println(p.name)
    fmt.Println(p.age)
}
```

![Simple](simple.gif)

In diesem Beispiel wird eine Struktur `person` definiert, die zwei Felder `name` und `age` enthält. Eine Variable `p` vom Typ `person` wird erstellt und initialisiert. Die Felder der Struktur können mit dem Punktoperator `.` zugegriffen werden.

Strukturen können auch anonyme Felder enthalten. Ein anonymes Feld ist ein Feld ohne Namen.

```go
package main

import "fmt"

type person struct {
    string
    int
}

func main() {
    p := person{"Alice", 30}
    fmt.Println(p)
    fmt.Println(p.string)
    fmt.Println(p.int)
}
```

![anonym](anonym.gif)
