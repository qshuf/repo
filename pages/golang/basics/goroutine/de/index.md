# Goroutines and Channels

GOLANG hat eine eingebaute Unterstützung für nebenläufige Programmierung. Ein Goroutine ist ein leichtgewichtiger Thread, der von der GOLANG-Runtime verwaltet wird. Um eine Goroutine zu starten, verwenden wir das Schlüsselwort `go` gefolgt von einer Funktionaufruf. Hier ist ein einfaches Beispiel:

``` { .go .copy }
package main

import (
    "fmt"
    "time"
)

func sayHello() {
    fmt.Println("Hello")
}

func main() {
    go sayHello()
    time.Sleep(100 * time.Millisecond)
}
```

![Hello](hello.gif)

In diesem Beispiel wird die Funktion `sayHello` in einer Goroutine gestartet. Die `main`-Funktion wartet 100 Millisekunden, bevor sie beendet wird. Wenn die `main`-Funktion beendet wird, wird auch die Goroutine beendet. Um dies zu verhindern, können wir einen `sync.WaitGroup` verwenden:

``` { .go .copy }
package main

import (
    "fmt"
    "sync"
)

func sayHello(wg *sync.WaitGroup) {
    fmt.Println("Hello")
    wg.Done()
}

func main() {
    var wg sync.WaitGroup
    wg.Add(1)
    go sayHello(&wg)
    wg.Wait()
}
```

In diesem Beispiel wird die `main`-Funktion auf die Goroutine warten, bis sie beendet ist. Die `sync.WaitGroup` wird verwendet, um die Anzahl der Goroutinen zu zählen, die noch ausgeführt werden. Wenn die Goroutine beendet ist, wird `wg.Done()` aufgerufen, um die Anzahl der Goroutinen zu verringern.

Channels sind eine Möglichkeit, Daten zwischen Goroutinen zu übertragen. Ein Channel ist ein Typ, der eine Richtung hat, entweder `chan<-` für das Senden oder `<-chan` für das Empfangen. Hier ist ein Beispiel:

``` { .go .copy }
package main

import "fmt"

func main() {
    ch := make(chan string)
    go func() {
        ch <- "Hello"
    }()
    msg := <-ch
    fmt.Println(msg)
}
```



In diesem Beispiel wird ein Channel `ch` erstellt, um eine Zeichenfolge zu übertragen. Eine Goroutine wird gestartet, um die Zeichenfolge "Hello" in den Channel zu senden. Die `main`-Funktion wartet, bis die Zeichenfolge empfangen wird, und druckt sie dann auf der Konsole aus.

Channels können auch mit einem optionalen Puffer erstellt werden, um mehrere Werte zu speichern. Hier ist ein Beispiel:

``` { .go .copy }
package main

import "fmt"

func main() {
    ch := make(chan string, 2)
    ch <- "Hello"
    ch <- "World"
    fmt.Println(<-ch)
    fmt.Println(<-ch)
}
```

![Hello](hello-world.gif)