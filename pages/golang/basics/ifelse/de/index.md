# If/Then/Else

Um in GOLANG entscheiden zu können, ob ein bestimmter Codeblock ausgeführt werden soll oder nicht, kannst du die `if`-Anweisung verwenden. Die `if`-Anweisung wird verwendet, um einen Codeblock auszuführen, wenn eine Bedingung wahr ist.

Um es ein wenig interactiv zu machen nutzen wir [pterm.sh]() für den Input.

```go
package main

import (
	"fmt"
	"strconv"

	"github.com/pterm/pterm"
)

func main() {
	// Create an interactive text input with single line input mode and show it
	var x int
	var err error
	for {
		result, _ := pterm.DefaultInteractiveTextInput.Show("Please insert a number: ")
		// Print a blank line for better readability
		pterm.Println()
		// Print the user's answer with an info prefix
		pterm.Info.Printfln("You answered: %s", result)
		x, err = strconv.Atoi(result)
		if err != nil {
			pterm.Error.Println("Please insert a valid number!")
		} else {
			break
		}
	}
	if x > 5 {
		fmt.Println("x ist größer als 5")
	} else {
		fmt.Println("x ist kleiner oder gleich 5")
	}
}
```
