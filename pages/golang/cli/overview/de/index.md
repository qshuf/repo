# Übersicht Golang

GOLANG ist eine typisierte Programmiersprache, die von Google entwickelt wurde. Sie ist eine der am schnellsten wachsenden Programmiersprachen und wird von vielen Entwicklern auf der ganzen Welt verwendet. GOLANG ist eine Open-Source-Programmiersprache, die eine einfache und effiziente Möglichkeit bietet, skalierbare und zuverlässige Software zu entwickeln.

Um ein Program auszuführen wird der Befehl `go run` verwendet. Dieser Befehl führt den Code aus, der in der Datei `main.go` geschrieben wurde.

```bash
cat > main.go <<EOF
package main

import "fmt"

func main() {
    fmt.Println("Hello, World!")
}
EOF
go run main.go
```

In diesem Beispiel wird der Text "Hello, World!" auf der Konsole ausgegeben.

```{ .bash .no-copy }
$ go run main.go
Hello, World!
```

Dabei wird zur Kompilierungszeit geprüft, ob der Code korrekt ist. Wenn der Code fehlerhaft ist, wird ein Fehler ausgegeben. Z.b. wenn wir vergessen das Paket `fmt` zu importieren.

```bash
cat > main.go <<EOF
package main

func main() {
    fmt.Println("Hello, World!")
}
EOF
go run main.go
```

Der Fehler wird wie folgt ausgegeben:

```{ .bash .no-copy }
go run main.go
# command-line-arguments
./main.go:4:5: undefined: fmt
```