# Hello World CLI

Nun ist das `Hello World` Program mit nur einer Zeile sehr rudimentär. Schreiben wir ein einfaches CLI-Programm, das eine Datei öffnet und ihren Inhalt auf der Konsole ausgibt.

## Datei einlesen

Um eine Datei zu öffnen und ihren Inhalt zu lesen, verwenden wir die Funktion `os.Open` und `io.ReadAll`.

```go
cat > main.go << EOF
package main

import (
    "fmt"
    "bufio"
    "os"
    "log"
)

func main() {
    file, err := os.Open("main.go")
    if err != nil {
        fmt.Println(err)
        return
    }
    defer file.Close()
    scanner := bufio.NewScanner(file)
    fmt.Println("##Dateiinhalt:")
    for scanner.Scan() {
        fmt.Println(scanner.Text())
    }
    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }
}
EOF
go run main.go
```

In diesem Beispiel wird der Inhalt der Datei `main.go` auf der Konsole ausgegeben. Klicke auf die plus Symbole, um die Erklärungen zu sehen.

```{ .bash .no-copy }
go run main.go
package main

import (
    "fmt"
    "bufio"
    "os"
    "log"
)

func main() {
    file, err := os.Open("main.go")  # (1)! 
    if err != nil {
        fmt.Println(err)
        return
    }
    defer file.Close() # (2)! 
    scanner := bufio.NewScanner(file) # (3)!
    for scanner.Scan() { # (4)!
        fmt.Println(scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }
}
```

1. `os.Open("main.go")` öffnet die Datei `main.go` im aktuellen Verzeichnis. Wenn ein Fehler auftritt, wird er in der Variablen `err` gespeichert.  
2. `defer file.Close()` schließt die Datei, wenn die Funktion `main` beendet wird.
3. `bufio.NewScanner(file)` erstellt einen Scanner, der den Inhalt der Datei zeilenweise liest.
4. `for scanner.Scan() { fmt.Println(scanner.Text()) }` liest jede Zeile der Datei und gibt sie auf der Konsole aus.
