# Swimlane AppSubmit
```mermaid
graph LR
    classDef red fill:#f01
    classDef green fill:#0f1
    A[issue created] --> CI(Create AddUserRequest);
    CI --> C1[Link issue to Swimlane];
    CI --> Q1{NotifyIntOffice?}
    Q1 -->|yes| C3[Notify IntOffice];
    C3 --> Comment[Create comment about email];
```