# Swimlane Preboarding

```mermaid
graph LR
    classDef red fill:#f01
    classDef green fill:#0f1
    A[issue created] --> CI(Create AddUserRequest);
    CI --> C1[Link issue to Swimlane];
    CI --> Q1{NotifyIntOffice?}
    Q1 -->|yes| C3[Notify IntOffice];
    C3 --> Comment[Create comment about email];
```

- [ ] Löschen des Postfaches 2 Wochen vor Antritt
- [ ] Wann und wo werden die verteiler eingestellt?
- [ ] Orbs Zugang für bestimmte Mitarbeiter bei Genehmigung (Tickets pro Mitarbeiter)

