# Adjust User

```mermaid
flowchart TB
    classDef red fill:#f01
    classDef green fill:#0f1
subgraph Z[" "]
direction TB
    C1[RequestType];
    C1 --> |correct| AC1;
    AC1{Username} -->|is empty| D1[Username=$issue.fields.Summary];
    AC1 -->|is not empty| E1[Username=$issue.fields.Summary];
    D1 --> E1[Summary='Update User $issue.fields.Summary'];
    E1 --> F1[Search Asset for $Username];
    F1 --> G1{ShowContact};
    G1 -->|no| H1[Update $asset.HideContact=true];
    G1 -->|yes| H2[Update $asset.HideContact=false];
    F1 --> I1{UpdatePhone?};
    I1 -->|yes| J1[Update $asset.Phone=$issue.Phone];
    F1 --> IA1{UpdateLocation?};
    IA1 -->|yes| AB1[Update $asset.Location=$issue.Location];
    H1--> T1[Transition CLOSED];
    H2 --> T1;
    J1 --> T1;
    AB1 --> T1;
    T1:::green;
end
S0[issue created]:::green --> S1;
S1{ReporterCheck} -->|failed| C[Comment Not allowed];
C --> D[Transition FAILED]
S1 -->|ok| C1;
D:::red;
```
