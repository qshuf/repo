# VSCode devcontainer

Um devcontainer nutzten zu können, benötigst du die [Remote - Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) Erweiterung für Visual Studio Code.

[Install extension](vscode:extension/ms-vscode-remote.remote-containers)

Nachdem die Extension installiert ist, erstelle einen `.devcontainers` Ordner in deinem Projekt und erstelle eine `devcontainer.json` Datei.
