# VSCode devcontainer

Um einen GOLANG devcontainer zu nutzen erstelle die Datei `devcontainer.json` in dem Ordner `.devcontainers`.

```json
{
"name": "Go",
// Or use a Dockerfile or Docker Compose file. More info: https://containers.dev/guide/dockerfile
"image": "mcr.microsoft.com/devcontainers/go:1-1.22-bullseye",

// Features to add to the dev container. More info: https://containers.dev/features.
// "features": {},

// Configure tool-specific properties.
"customizations": {
    // Configure properties specific to VS Code.
    "vscode": {
        "settings": {},
        "extensions": [
            "streetsidesoftware.code-spell-checker"
        ]
    }
},

// Use 'forwardPorts' to make a list of ports inside the container available locally.
// "forwardPorts": [9000],
// Uncomment to connect as root instead. More info: https://aka.ms/dev-containers-non-root.
"remoteUser": "root",
// Use 'portsAttributes' to set default properties for specific forwarded ports. 
// More info: https://containers.dev/implementors/json_reference/#port-attributes
"portsAttributes": {
    "9000": {
        "label": "Hello Remote World",
        "onAutoForward": "notify"
    }
}
```
