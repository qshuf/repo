# Workshop Conventions

This workshop is assembled with [qshuf](https://gitlab.com/qnib-golang/qshuf) and build using [mkdocs-material](https://squidfunk.github.io/mkdocs-material/). 
To make sure you make the most out of the workshop, let's go through some of the features of the workshop.

## Navigation

Each chapter has it's own top level navigation entry (**(1)**). The navigation within the chapter is done via the left sidebar(**(2)**).

![Navigation](navigation.png)

## Annotations

A cool feature of mkdocs-material is the ability to annotate just about anything. If you find little plus icons, you can click on them to get more information.

``` { .bash .no-copy }
$ docker run -ti \ # (1)
            --rm \ # (2)
            alpine:latest \
            echo Hello World
Hello World

1. `-t` (`--tty`) captures the output (stderr/stdout) using a pseudo-TTY. In simple terms it attaches a terminal to represent the output.
    `-i` (`--input`) attaches stdin, so that you can actually type or pipe input into the container.
2. `--rm` is going to remove the container from the system once it finshes.
```

Results in:

``` { .bash .no-copy }
$ docker run -ti \ # (1)
            --rm \ # (2)
            alpine:latest \
            echo Hello World
Hello World
```

1. `-t` (`--tty`) captures the output (stderr/stdout) using a pseudo-TTY. In simple terms it attaches a terminal to represent the output.
    `-i` (`--input`) attaches stdin, so that you can actually type or pipe input into the container.
2. `--rm` is going to remove the container from the system once it finshes.

## Code-Blocks

Throughtout the workshop you'll find code blocks like this:

```{ .bash .copy }
docker run -ti --rm alpine:3.18 echo Hello World
```

You will notice that there is a faint copy-to-clipboard icon at the very top right. If you click on it, it will copy the code block to your clipboard. This is useful if you want to paste the code into your terminal.

![Code-Block](code-block-annotation.gif)

Each time you see a code-block you are meant to execute, you are supposed to also find a code block that shows the output. As annotations most likely break the command itself, we'll annotate the output in a separate code block. Output code blocks won't have the copy icon at the top right to indicate that you are not supposed to copy the output.

```{ .bash .no-copy .annotate }
$ docker run -ti \ # (1)!
            --rm \ # (2)!
            alpine:latest \
            echo Hello World
Hello World
```

1. `-t` (`--tty`) captures the output (stderr/stdout) using a pseudo-TTY. In simple terms it attaches a terminal to represent the output.
    `-i` (`--input`) attaches stdin, so that you can actually type or pipe input into the container.
2. `--rm` is going to remove the container from the system once it finshes.

!!! tipp "Code blocks and annotations"
    Make it a habit to always use the copy-icon to copy the code-blocks to your clipboard. This will save you a lot of time and typos and if you do not see a copy-icon you are not supposed to copy it in the first place. :smile:
